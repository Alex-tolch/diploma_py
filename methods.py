import json
import requests
import os
import shutil
import paramiko
import time
import random
import re
from requests.structures import CaseInsensitiveDict

ssh = paramiko.SSHClient()


def update_json_data(path, params):
    json_file = get_vars_from_json(path)
    json_file.update(params)
    set_vars_to_json(path, json_file)

def get_iam_token(path, url):
    token_url = url
    data = get_vars_from_json(path)
    output = requests.post(token_url, params=data)
    token = output.json()["iamToken"]
    return token, output.status_code

def set_vars_to_json(path, params):
    with open(path, 'w') as f:
        json.dump(params, f)

def get_vars_from_json(path):
    json_file = open(path)
    variables = json.load(json_file)
    json_file.close()
    return variables

def create_folder(path, url, headers):
    folder_url = url
    data = get_vars_from_json(path)
    output = requests.post(folder_url, params=data, headers=headers)
    folder_id = output.json()["metadata"]["folderId"]
    return folder_id, output.status_code

def get_cloud_id(url, headers):
    cloud_url = url
    output = requests.get(cloud_url, headers=headers)
    cloud_id = output.json()["clouds"][0]["id"]
    return cloud_id, output.status_code

def get_network_id(path, url, headers):
    network_url = url
    data = get_vars_from_json(path)
    output = requests.post(network_url, params=data, headers=headers)
    network_id = output.json()["metadata"]["networkId"]
    return network_id, output.status_code

def get_subnet_id(path, url, headers):
    subnet_url = url
    data = get_vars_from_json(path)
    output = requests.post(subnet_url, params=data, headers=headers)
    sunbet_id = output.json()["metadata"]["subnetId"]
    return sunbet_id, output.status_code

def get_instance_id(path, url, headers):
    instance_url = url
    data = get_vars_from_json(path)
    output = requests.post(instance_url, json=json.loads(json.dumps(data)), headers=headers)
    instance_id = output.json()["metadata"]["instanceId"]
    return instance_id, output.status_code

def get_instance_ip(url, headers):
    instance_url = url
    output = requests.get(instance_url, headers=headers)
    instance_ip = output.json()["networkInterfaces"][0]["primaryV4Address"]["oneToOneNat"]["address"]
    return instance_ip, output.status_code

####generate key
def generate_keys_in_folder(output_dir = "./output"):
    key_name = "id_key"
    cur_dir = os.path.dirname(os.path.abspath(__file__))
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    if not os.listdir(output_dir):
        os.system(f"ssh-keygen -t rsa -b 2048 -q -N '' -f {cur_dir}\\{output_dir}\\{key_name}")
    else:
        shutil.rmtree(output_dir)
        os.mkdir(output_dir)
        os.system(f"ssh-keygen -t rsa -b 2048 -q -N '' -f {cur_dir}\\{output_dir}\\{key_name}")
    keys_folder=f"{cur_dir}\\{output_dir}"
    return keys_folder, key_name

def get_public_key(keys_folder, key_name):
    with open(f"{keys_folder}\\{key_name}.pub", "r") as f:
        data = f.read()
    return str(data)

def get_private_key(path):
    with open(path, 'r') as private_key:
        pkey = private_key.read()
    return pkey

def replace(token, cloud, folder):
    # Read contents from file as a single string
    file_handle = open("./terraform/sample.tf", 'r')
    file_string = file_handle.read()
    file_handle.close()
    # Use RE package to allow for replacement (also allowing for (multiline) REGEX)
    file_string = (re.sub("FOLDER", folder, file_string))
    file_string = (re.sub("CLOUD", cloud, file_string))
    file_string = (re.sub("TOKEN", token, file_string))
    # Write contents to file.
    # Using mode 'w' truncates the file.
    file_handle = open("./terraform/main.tf", 'w+')
    file_handle.write(file_string)
    file_handle.close()




def run_command_on_device(ip_address, username, pkey, command):
    # Load SSH host keys.
    ssh.load_system_host_keys()
    # Add SSH host key when missing.
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    total_attempts = 10
    result_connect = False
    for attempt in range(total_attempts):
        try:
            print("Attempt to connect: %s" % attempt)
            # Connect to router using username/password authentication.
            ssh.connect(ip_address, 
                        username=username, 
                        pkey=pkey,
                        look_for_keys=False 
                        )
            # Run command.
            print(f"Start runing command: {command}")
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
            # Read output from command.
            output = ssh_stdout.readlines()
            # inpt = ssh_stdin.readlines()
            # err = ssh_stderr.readlines()
            # print_logs(inpt)
            print_logs(output)
            # print(err)
            # Close connection.
            print(*output,sep='\n')
            ssh.close()
            result_connect = True
            return result_connect
        except Exception as error_message:
            print("Try to connect...")
            time.sleep(10)
            print(error_message)
            print_logs(error_message)

def create_log_dir():
    log_dir = "./logs/"
    shutil.rmtree(log_dir)
    os.mkdir(log_dir)

def print_logs(input):
    log_dir = "./logs/"
    if isinstance(input, list):
        str1 = ''.join(str(e) for e in input)
        input = str1
    with open(log_dir+"logs.txt", "a+") as myfile:
        myfile.write(str(input)+"\n")

token = get_iam_token("./jsons/get_aim_token.json", "https://iam.api.cloud.yandex.net/iam/v1/tokens")[0]

headers = CaseInsensitiveDict()
headers["Accept"] = "application/json"
headers["Authorization"] = "Bearer "+token
headers["Content-Type"] = "application/json"


OAuth_token = "AQAAAAAY0H_mAATuwQJuItakBUyCsUf7D5AsO5s"


##############url
token_url = "https://iam.api.cloud.yandex.net/iam/v1/tokens"
cloud_url = "https://resource-manager.api.cloud.yandex.net/resource-manager/v1/clouds"
folder_url = "https://resource-manager.api.cloud.yandex.net/resource-manager/v1/folders"
network_url = "https://vpc.api.cloud.yandex.net/vpc/v1/networks"
subnet_url = "https://vpc.api.cloud.yandex.net/vpc/v1/subnets"
instance_url = "https://compute.api.cloud.yandex.net/compute/v1/instances"
repo_url = "https://gitlab.com/Alex-tolch/diploma.git"
#############paths
get_aim_token_json = "./jsons/get_aim_token.json"
create_folder_json = "./jsons/create_folder.json"
create_network_json = "./jsons/create_network.json"
create_subnet_json = "./jsons/create_subnet.json"
create_instance_json = "./jsons/create_instance.json"
#############commands
git_install = f"sudo apt install git -y"
git_clone = f"git clone {repo_url}"
preinstall_sh = f"sh /home/ubuntu/diploma/preinstall.sh"
ansible_main = f"ansible-playbook /home/ubuntu/diploma/ansibleProject/main.yaml -i /home/ubuntu/diploma/ansibleProject/hosts"
remote_tf_file = f"/home/ubuntu/diploma/terraform_files/main.tf"
rm_tf_file = f"ansible-playbook /home/ubuntu/diploma/terraform_files/main.tf"
ansible_conf = f"export ANSIBLE_CONFIG=/home/ubuntu/diploma/ansibleProject/ansible.cfg"
ansible_conf_copy = f"cp /home/ubuntu/diploma/ansibleProject/ansible.cfg /home/ubuntu/.ansible.cfg"


index = 1
error = "unexpected error"
create_log_dir()
while index != 9:
    match index:
        case 1:
            try:
                token = get_iam_token(get_aim_token_json, token_url)
                if token[1] == 200:
                    token = token[0]
                    index = 2
                    print(token)
                    print_logs(token)
                else:
                    error = "token request code: "+str(token[0])
                    break
            except:
                print(error+f": {token_url}")
                break
        case 2:
            try:
                cloud_id = get_cloud_id(cloud_url, headers)
                if cloud_id[1] == 200:
                    cloud_id = cloud_id[0]
                    index = 3
                    print("cloudid: "+str(cloud_id))
                    print_logs("cloudid: "+str(cloud_id))
                else:
                    error = "cloud_id request code: "+str(cloud_id[0])
                    break
            except:
                print(error+f": {cloud_url}")
                break
        case 3:
            try:
                foder_name = "folder-"+str(random.randint(0, 1000))
                params = {"cloudId": f"{cloud_id}", "name": f"{foder_name}"}
                update_json_data(create_folder_json, params)
                folder_id = create_folder(create_folder_json, str(folder_url), headers)
                if folder_id[1] == 200:
                    folder_id = folder_id[0]
                    index = 4
                    print("folder_id: "+str(folder_id))
                    print_logs("folder_id: "+str(folder_id))
                else:
                    error = "folder_id request code: "+str(folder_id[0])
                    break
            except:
                print(error+f": {folder_url}")
                break
        case 4:
            try:
                params = {"folderId": f"{folder_id}", "name": "other"}
                update_json_data(create_network_json, params)
                network_id = get_network_id(create_network_json, str(network_url), headers)
                if network_id[1] == 200:
                    network_id = network_id[0]
                    index = 5
                    print("network_id: "+str(network_id))
                    print_logs("network_id: "+str(network_id))
                else:
                    error = "network_id request code: "+str(network_id[0])
                    break    
            except:
                print(error+f": {network_url}")
                break
        case 5:
            try:
                params = {"folderId": f"{folder_id}", "networkId": f"{network_id}"}
                update_json_data(create_subnet_json, params)
                subnet_id = get_subnet_id(create_subnet_json, str(subnet_url), headers)
                if subnet_id[1] == 200:
                    subnet_id = subnet_id[0]
                    index = 6
                    print("subnet_id: "+str(subnet_id))
                    print_logs("subnet_id: "+str(subnet_id))
                else:
                    error = "subnet_id request code: "+str(subnet_id[0])
                    break    
            except:
                print(error+f"\n{subnet_id}")
                break
        case 6:
            try:
                key_folder = generate_keys_in_folder()
                public_key = get_public_key(key_folder[0], key_folder[1])
                params = {"folderId": f"{folder_id}", "metadata": {"serial-port-enable": "1", "ssh-keys": f"ubuntu:{public_key}"}, "networkInterfaceSpecs": [{"subnetId": f"{subnet_id}","primaryV4AddressSpec": {"oneToOneNatSpec": {"ipVersion": "IPV4"}}}]}
                update_json_data(create_instance_json, params)
                instance_id = get_instance_id(create_instance_json, str(instance_url), headers)
                if instance_id[1] == 200:
                    instance_id = instance_id[0]
                    index = 7
                    print("instance_id: "+str(instance_id))
                    print_logs("instance_id: "+str(instance_id))
                else:
                    error = "instance_id request code: "+str(instance_id[0])
                    break    
            except:
                print(error+f": {instance_url}")
                break
        case 7:
            try:
                time.sleep(60)
                instance_ip = get_instance_ip(str(instance_url+f"/{instance_id}"), headers)
                if instance_ip[1] == 200:
                    instance_ip = instance_ip[0]
                    index = 8
                    print("instance_ip: "+str(instance_ip))
                    print_logs("jump_host_ip: "+str(instance_ip))
                else:
                    error = "instance_id request code: "+str(instance_ip[0])
                    break    
            except:
                print(error+f": {instance_ip}")
                break
        case 8:
            k = paramiko.RSAKey.from_private_key_file("./output/id_key")
            com_index = 1
            while com_index != 6:
                match com_index:
                    case 1:
                        try:
                            if run_command_on_device(instance_ip, "ubuntu", k, git_install):
                                com_index = 2
                            else:
                                print(f"error with executing \"{git_install}\" command")
                                break
                        except:
                            print(error)
                            break
                    case 2:
                        try:
                            if run_command_on_device(instance_ip, "ubuntu", k, git_clone):
                                com_index = 3
                            else:
                                print(f"error with executing \"{git_clone}\" command")  
                                break
                        except:
                            print(error)
                            break
                    case 3:
                        try:
                            if run_command_on_device(instance_ip, "ubuntu", k, preinstall_sh):
                                com_index = 4
                            else:
                                print(f"error with executing \"{preinstall_sh}\" command")
                                break
                        except:
                            print(error)
                            break
                    case 4:
                        try:
                            if run_command_on_device(instance_ip, "ubuntu", k, preinstall_sh):
                                replace(OAuth_token, cloud_id, folder_id)
                                run_command_on_device(instance_ip, "ubuntu", k, f"rm {remote_tf_file}")
                                run_command_on_device(instance_ip, "ubuntu", k, f"{ansible_conf}")
                                run_command_on_device(instance_ip, "ubuntu", k, f"{ansible_conf_copy}")
                                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                                ssh.connect(hostname=f"{instance_ip}", username="ubuntu", pkey=k, look_for_keys=False)
                                sftp = ssh.open_sftp()
                                sftp.put("./terraform/main.tf", remote_tf_file)
                                sftp.close()
                                com_index = 5
                            else:
                                print(f"error with executing \"{remote_tf_file}, {ansible_conf}, {ansible_conf_copy}\" commands")
                                break
                        except:
                            print(error)
                            break
                    case 5:
                        try:
                            if run_command_on_device(instance_ip, "ubuntu", k, ansible_main):
                                com_index = 6
                            else:
                                print(f"error with executing \"{ansible_main}\" command")
                                break
                        except:
                            print(error)
                            break
            index = 9
